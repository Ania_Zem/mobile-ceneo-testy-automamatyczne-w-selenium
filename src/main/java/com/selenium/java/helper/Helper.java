package com.selenium.java.helper;

import com.selenium.java.base.BaseTest;
import com.selenium.java.base.TestListener;
import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import java.lang.reflect.Array;
import java.time.Duration;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class Helper extends BaseTest {

    public void moveIt(double start_x, double start_y, double end_x, double end_y) {
        WebDriver driver = getDriver();
        new TouchAction((PerformsTouchActions) driver).longPress(PointOption.point((int) start_x, (int) start_y))
                .waitAction(WaitOptions.waitOptions(Duration.ofMillis(1500)))
                .moveTo(PointOption.point((int) end_x, (int) end_y)).release().perform();
    }

    public void longPress(double point_x, double point_y) {
        WebDriver driver = getDriver();
        new TouchAction((PerformsTouchActions) driver).longPress(PointOption.point((int) point_x, (int) point_y)).perform();
    }

    public void pauza(int i) {
        try {
            Thread.sleep(i);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void comeBack() {
        WebDriver driver = getDriver();
        ((AndroidDriver) driver).pressKey(new KeyEvent(AndroidKey.BACK));
    }

    public void tapCoordinates(double point_x, double point_y) {

        WebDriver driver = getDriver();
        new TouchAction((PerformsTouchActions) driver).tap(PointOption.point((int) point_x, (int) point_y)).perform();
    }

    public enum direction {
        DOWN,
        UP,
        LEFT,
        RIGHT
    }

    private int screenHeight;
    private int screenWidth;

    public int[] getResolutionHandler() {
        WebDriver driver = getDriver();
        Dimension size;
        size = driver.manage().window().getSize();
        screenHeight = (size.height);
        screenWidth = (size.width);
        return new int[]{screenHeight, screenWidth};
    }

    /*
    default value of power is 0.8, should not be less than 0.2
    */
    public void swipeInDirection(direction direction, String place, double power) {

        WebDriver driver = getDriver();

        int res[] = getResolutionHandler();
        int screenHeight = (int) Array.get(res, 0);
        int screenWidth = (int) Array.get(res, 1);
        int startY, startX, endX, endY;
        double multiplierX;
        double multiplierY;
        switch (place) {
            case "left":
                multiplierX = 0.2;
                multiplierY = 0.5;
                break;
            case "right":
                multiplierX = 0.8;
                multiplierY = 0.5;
                break;
            case "up":
                multiplierX = 0.5;
                multiplierY = 0.2;
                break;
            case "down":
                multiplierX = 0.5;
                multiplierY = 0.8;
                break;
            default:
                multiplierX = 0.5;
                multiplierY = 0.5;
                break;
        }
        switch (direction) {
            case DOWN:
                startX = (int) (screenWidth * multiplierX);
                startY = (int) (screenHeight * 0.2);
                endX = (int) (screenWidth * multiplierX);
                endY = (int) (screenHeight * power);
                break;
            case UP:
                startX = (int) (screenWidth * multiplierX);
                startY = (int) (screenHeight * power);
                endX = (int) (screenWidth * multiplierX);
                endY = (int) (screenHeight * 0.2);
                break;
            case RIGHT:
                startX = (int) (screenWidth * power);
                startY = (int) (screenHeight * multiplierY);
                endX = (int) (screenWidth * 0.2);
                endY = (int) (screenHeight * multiplierY);
                break;
            case LEFT:
                startX = (int) (screenWidth * 0.2);
                startY = (int) (screenHeight * multiplierY);
                endX = (int) (screenWidth * power);
                endY = (int) (screenHeight * multiplierY);
                break;
            default:
                throw new IllegalArgumentException("Incorrect direction: " + direction);
        }
        new TouchAction((PerformsTouchActions) driver).press(PointOption.point(startX, startY))
                .waitAction(WaitOptions.waitOptions(Duration.ofMillis(1000)))
                .moveTo(PointOption.point(endX, endY)).release().perform();
    }

    public void swipeToActionPartners() {
        int i = 0;
        while (swipeToElementByXPATH("//*[contains(@text, 'Partnerzy akcji')]") && i < 6) {

            swipeInDirection(direction.UP, "UP", 0.7);
            i++;
        }
    }

    public void testScreenShoot(String testname) {
        WebDriver driver = getDriver();
        TestListener testListener = new TestListener();
        if (driver instanceof WebDriver) {
            testListener.saveScreenshotPNG(driver);
        }
        testListener.saveTextLog("Test " + testname + " end and screenshoot taken!");
    }

    public void getScreenShot(String text) {
        WebDriver driver = getDriver();
        File file = new File("imagesFromTests/");
        String imagePath = file.getAbsolutePath();
        File srcFile;
        File targetFile;
        String pathToNewFile = imagePath + "/" + text;
        srcFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        targetFile = new File(pathToNewFile);
        try {
            FileUtils.copyFile(srcFile, targetFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void swipeDown3Times() {
        int i = 0;
        while (swipeToElementByXPATH("//*[contains(@text, 'Partnerzy akcji')]") && i < 3) {

            swipeInDirection(direction.UP, "UP", 0.9);
            i++;
        }
    }


    public void swipeToSimilarProduct() {

        int i = 0;
        while (swipeToElementById("pl.ceneo:id/recommendationsLL") && i < 4) {
            swipeInDirection(direction.UP, "UP", 0.9);
            i++;
        }
    }

    public static boolean swipeToElementByText(String text) {
        WebDriverWait wait = getWaitDriver(1);
        try {
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[contains(@text, '" + text + "')]")));
            return false;
        } catch (Exception e) {
            return true;
        }
    }

}