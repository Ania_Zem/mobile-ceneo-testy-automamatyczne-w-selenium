package Android;

import com.selenium.java.base.BaseTest;
import com.selenium.java.base.TestListener;
import com.selenium.java.helper.Helper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;


public class Search_MainCeneoScreen extends BaseTest {

    @FindBy(how = How.ID, using = "pl.ceneo:id/barcodeMenuItem")
    public WebElement codeScannerBtn;
    @FindBy(how = How.ID, using = "pl.ceneo:id/capture_via_visual")
    public WebElement findByPhoto;
    @FindBy(how = How.ID, using = "pl.ceneo:id/visual_search_capture")
    public WebElement makeFotoBtn;
    @FindBy(how = How.ID, using = "pl.ceneo:id/right_button")
    public WebElement acceptBtn;
    @FindBy(how = How.ID, using = "pl.ceneo:id/viewTypeItem")
    public WebElement viewBtn;
    @FindBy(how = How.ID, using = "pl.ceneo:id/capture_via_barcode")
    public WebElement bracodeBtn;
    @FindBy(how = How.ID, using = "pl.ceneo:id/manualCodeButton")
    public WebElement manualCodeBtn;
    @FindBy(how = How.ID, using = "pl.ceneo:id/qrCodeContent")
    public WebElement codeField;
    @FindBy(how = How.ID, using = "android:id/button1")
    public WebElement acceptCodeBtn;
    @FindBy(how = How.ID, using = "pl.ceneo:id/pageLink3")
    public WebElement productOpinionsBtn;
    @FindBy(how = How.ID, using = "pl.ceneo:id/addReviewButton")
    public WebElement addProductOpinionBtn;
    @FindBy(how = How.ID, using = "pl.ceneo:id/ok")
    public WebElement acceptRulesBtn;
    @FindBy(how = How.ID, using = "pl.ceneo:id/ratingbar")
    public WebElement productRatingBtn;
    @FindBy(how = How.ID, using = "pl.ceneo:id/upvotesIV")
    public WebElement ratingEffectivenessBtn;
    @FindBy(how = How.ID, using = "pl.ceneo:id/downvotesIV")
    public WebElement ratingProductivityBtn;
    @FindBy(how = How.ID, using = "pl.ceneo:id/upvotesIV")
    public WebElement ratingSmellBtn;
    @FindBy(how = How.ID, using = "pl.ceneo:id/opinionText")
    public WebElement opinionField;
    @FindBy(how = How.ID, using = "pl.ceneo:id/accountEditNickname")
    public WebElement addingNickField;
    @FindBy(how = How.ID, using = "pl.ceneo:id/sendOpinionButton")
    public WebElement sendOpinionButton;
//



//    @FindBy(how = How.XPATH, using = "//*[contains(@text,'Cena')]")
//    public WebElement bbb;


    WebDriver driver = BaseTest.getDriver();
    Helper helper = PageFactory.initElements(driver, Helper.class);
    TestListener testListener = PageFactory.initElements(driver, TestListener.class);

    public void codeScannerByFoto(){
        helper.pauza(1200);
        codeScannerBtn.click();
        helper.pauza(800);
        findByPhoto.click();
        helper.pauza(1000);
        makeFotoBtn.click();
        helper.pauza(1200);
        acceptBtn.click();
        helper.pauza(3500);

        helper.testScreenShoot("Szukanie za pomocą obrazu");
        System.out.println("Zrobiony screen szukania za pomocą obrazu");
        helper.pauza(1000);

        viewBtn.click();
        helper.swipeDown3Times();


    }

    public void codeScannerByNumbers() {
        helper.pauza(1200);
        codeScannerBtn.click();
        helper.pauza(800);
        bracodeBtn.click();
        manualCodeBtn.click();
        codeField.sendKeys("5900525057845");
        acceptCodeBtn.click();


        helper.testScreenShoot("Błędny kod kreskowy");
        System.out.println("Zrobiony screen szukania za pomocą błędnego kodu kreskowego");
        helper.pauza(1000);

        acceptCodeBtn.click();

        codeScannerBtn.click();
        helper.pauza(800);
        bracodeBtn.click();
        manualCodeBtn.click();
        codeField.sendKeys("5900525057846");
        acceptCodeBtn.click();


        helper.testScreenShoot("Szukanie za pomocą kodu kreskowego");
        System.out.println("Zrobiony screen szukania za pomocą kodu kreskowego");
        helper.pauza(1000);

        helper.swipeToSimilarProduct();
    }
    public void addProductOpinion (){

        productOpinionsBtn.click();
        helper.pauza(700);
        addProductOpinionBtn.click();
        helper.testScreenShoot("Warunki dodania opinii");
        System.out.println("Zrobiony screen warunków dodania opinii");

        acceptRulesBtn.click();
        productRatingBtn.click();
        ratingEffectivenessBtn.click();
        helper.pauza(800);
        ratingProductivityBtn.click();
//        ratingSmellBtn.click();
        opinionField.sendKeys("Nie fajny");
        addingNickField.sendKeys("testerki");

//        sendOpinionButton.click();
        helper.testScreenShoot("Opinia nie spełniająca warunków");
        System.out.println("Zrobiony screen dodania złej opinii");
        helper.pauza(1000);

        acceptCodeBtn.click();


















//        helper.comeBack();


    }

}
