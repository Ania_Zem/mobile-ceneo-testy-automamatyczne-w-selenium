package com.selenium.java.pages.android;

import com.selenium.java.base.BaseTest;
import com.selenium.java.helper.Helper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class Search_CategoryScreen extends BaseTest {
    @FindBy(how = How.XPATH, using = "//*[contains(@text,'Komputery')]")
    public WebElement pcCategoryBtn;
    @FindBy(how = How.XPATH, using = "//*[contains(@text,'Oprogramowanie')]")
    public WebElement softwareCategoryBtn;
    @FindBy(how = How.XPATH, using = "//*[contains(@text,'Edytory grafiki i video')]")
    public WebElement editorsCategoryBtn;
    @FindBy(how = How.ID, using = "pl.ceneo:id/bar_title")
    public WebElement barTitle;
    @FindBy(how = How.XPATH, using = "//*[contains(@text,'Księgarnia')]")
    public WebElement bookCategoryBtn;
    @FindBy(how = How.XPATH, using = "//*[contains(@text,'Książki')]")
    public WebElement realBooksCategoryBtn;
    @FindBy(how = How.XPATH, using = "//*[contains(@text,'Informatyka')]")
    public WebElement itBooksCategoryBtn;
    @FindBy(how = How.XPATH, using = "//*[contains(@text,'Aplikacje biurowe')]")
    public WebElement appsCategoryBtn;


    WebDriver driver = BaseTest.getDriver();
    Helper helper = PageFactory.initElements(driver, Helper.class);

    public void computerOffertCategory() {

        pcCategoryBtn.click();
        helper.pauza(800);
        softwareCategoryBtn.click();
        helper.pauza(800);
        editorsCategoryBtn.click();
        helper.pauza(800);
        helper.testScreenShoot("Szukanie za pomocą kategorii \"Komputery\"");
        System.out.println("Zrobiony screen szukania za pomocą kategorii \"Komputery\"");
    }

    public void booksOffertCategory() {
        bookCategoryBtn.click();
        helper.pauza(800);
        realBooksCategoryBtn.click();
        helper.pauza(500);
        itBooksCategoryBtn.click();
        helper.pauza(500);
        appsCategoryBtn.click();
        helper.pauza(800);

        helper.testScreenShoot("Szukanie za pomoca kategorii Księgarnia");
        System.out.println("Zrobiony screen szukania za pomocą kategorii Księgarnia");
    }

}

