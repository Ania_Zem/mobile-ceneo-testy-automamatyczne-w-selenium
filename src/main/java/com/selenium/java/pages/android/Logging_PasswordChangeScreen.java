package com.selenium.java.pages.android;

import com.selenium.java.base.BaseTest;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
public class Logging_PasswordChangeScreen extends BaseTest {
    @FindBy(how = How.XPATH, using = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget." +
            "FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/androidx.drawerlayout.widget." +
            "DrawerLayout/android.widget.RelativeLayout/android.widget.FrameLayout[2]/android.widget.RelativeLayout" +
            "/android.widget.ScrollView/android.widget.RelativeLayout/android.widget.LinearLayout[8]/android.widget.RelativeLayout\n")
    public WebElement btn_PasswordChange;
    @FindBy(how = How.ID, using = "pl.ceneo:id/password")
    public WebElement txt_CurrentPassword;
    @FindBy(how = How.ID, using = "pl.ceneo:id/newPassword")
    public WebElement txt_NewPassword;
    @FindBy(how = How.ID, using = "pl.ceneo:id/repeatPassword")
    public WebElement txt_RepeatPassword;
    @FindBy(how = How.ID, using = "pl.ceneo:id/applyBtn")
    public WebElement btn_ApplyPassword;
    @FindBy(how = How.XPATH, using = "(//android.widget.ImageButton[@content-desc=\"Show password\"])[1]\n")
    public WebElement btn_ShowPassword1;
    @FindBy(how = How.XPATH, using = "(//android.widget.ImageButton[@content-desc=\"Show password\"])[2]\n")
    public WebElement btn_ShowPassword2;
    @FindBy(how = How.XPATH, using = "(//android.widget.ImageButton[@content-desc=\"Show password\"])[3]\n")
    public WebElement btn_ShowPassword3;


    public void myPasswordChange() {
        btn_PasswordChange.click();
        btn_ShowPassword1.click();
        btn_ShowPassword2.click();
        btn_ShowPassword3.click();
//        1 Nieprawidłowe obecne hasło
        txt_CurrentPassword.sendKeys("ObecneHasło");
        txt_NewPassword.sendKeys("NoweHasło1");
        txt_RepeatPassword.sendKeys("NoweHasło1");
        btn_ApplyPassword.click();
//        2 Różne nowe hasła
        txt_CurrentPassword.sendKeys("TesT1234!test");
        txt_NewPassword.sendKeys("NoweHasło1");
        txt_RepeatPassword.sendKeys("NoweHasło2");
        btn_ApplyPassword.click();
//        3 Krótkie nowe hasło
        txt_CurrentPassword.sendKeys("TesT1234!test");
        txt_NewPassword.sendKeys("NH1");
        txt_RepeatPassword.sendKeys("NH1");
        btn_ApplyPassword.click();
//        4 Długie nowe hasło
        txt_CurrentPassword.sendKeys("TesT1234!test");
        txt_NewPassword.sendKeys("NoweHasłoNoweHasło1");
        txt_RepeatPassword.sendKeys("NoweHasłoNoweHasło1");
        btn_ApplyPassword.click();
//        5 Dziwne zanaki w nowym haśle
        txt_CurrentPassword.sendKeys("TesT1234!test");
        txt_NewPassword.sendKeys("Nowe.!>Hasło1");
        txt_RepeatPassword.sendKeys("Nowe.!>Hasło1");
        btn_ApplyPassword.click();
//        6 Wszyskie hasła takie same
        txt_CurrentPassword.sendKeys("TesT1234!test");
        txt_NewPassword.sendKeys("TesT1234!test");
        txt_RepeatPassword.sendKeys("TesT1234!test");
        btn_ApplyPassword.click();
//        7 Poprawne obecne hasło i inne poprawne nowe hasło
        txt_CurrentPassword.sendKeys("TesT1234!test");
        txt_NewPassword.sendKeys("Testowanie1234!");
        txt_RepeatPassword.sendKeys("Testowanie1234!");
        btn_ApplyPassword.click();
    }
}