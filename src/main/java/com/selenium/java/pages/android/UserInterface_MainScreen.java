package com.selenium.java.pages.android;


import com.selenium.java.base.BaseTest;
import com.selenium.java.helper.Helper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class UserInterface_MainScreen extends BaseTest {

    @FindBy(how = How.XPATH, using = "//android.widget.ImageButton[@content-desc=\"menu otwarte\"]\n")
    public WebElement btn_Menu;
    @FindBy(how = How.XPATH, using = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget." +
            "FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/androidx.drawerlayout.widget.DrawerLayout" +
            "/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.RelativeLayout[2]\n")
    public WebElement btn_MyCeneo;
    @FindBy(how = How.ID, using = "pl.ceneo:id/ceneo_login_button")
    public WebElement btn_LoginCeneo;
    @FindBy(how = How.ID, using = "pl.ceneo:id/loginTextView")
    public WebElement txt_Email;
    @FindBy(how = How.ID, using = "pl.ceneo:id/passwordTextView")
    public WebElement txt_Password;
    @FindBy(how = How.ID, using = "pl.ceneo:id/loginButton")
    public WebElement btn_LoginButton;


    WebDriver driver = BaseTest.getDriver();
    Helper helper = PageFactory.initElements(driver, Helper.class);
    public void myUserInterface() {

        btn_Menu.click();
        btn_MyCeneo.click();
        btn_LoginCeneo.click();
        txt_Email.sendKeys("test.paulina@wp.pl");
        txt_Password.sendKeys("TesT1234!test");
        btn_LoginButton.click();
        helper.pauza(2000);
    }
}