package com.selenium.java.pages.android;

import com.selenium.java.base.BaseTest;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class Logging_RegistrationScreen extends BaseTest {

    @FindBy(how = How.XPATH, using = "//android.widget.ImageButton[@content-desc=\"menu otwarte\"]\n")
    public WebElement btn_menu;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Zaloguj')]")
    public WebElement btn_log;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'ZAREJESTRUJ SIĘ')]")
    public WebElement btn_reg;

    @FindBy(how = How.ID, using = "pl.ceneo:id/email")
    public WebElement input_email;

    @FindBy(how = How.ID, using = "pl.ceneo:id/newPassword")
    public WebElement input_pass;

    @FindBy(how = How.XPATH, using = "(//android.widget.ImageButton[@content-desc=\"Show password\"])[1]")
    public WebElement btn_show;

    @FindBy(how = How.ID, using = "pl.ceneo:id/repeatPassword")
    public WebElement input_morepass;

    @FindBy(how = How.ID, using = "pl.ceneo:id/acceptPersonalDataConsent")
    public WebElement btn_approve;

    @FindBy(how = How.ID, using = "pl.ceneo:id/applyBtn")
    public WebElement btn_apply;

    public void registrationToApp(String email, String pass, String morepass) {

        btn_menu.click();
        btn_log.click();
        btn_reg.click();
        input_email.sendKeys(email);
        input_pass.sendKeys(pass);
        btn_show.click();
        input_morepass.sendKeys(morepass);
        btn_approve.click();
        btn_apply.click();

    }

}
