package com.selenium.java.pages.android;

import com.selenium.java.base.BaseTest;
import com.selenium.java.helper.Helper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class Search_FindingCatsToys extends BaseTest{
    @FindBy(how = How.ID, using = "pl.ceneo:id/searchMenuItem")
    public WebElement findBtn;
    @FindBy(how = How.ID, using = "pl.ceneo:id/search_src_text")
    public WebElement searchTextField;
    @FindBy(how = How.ID, using = "pl.ceneo:id/productInfo")
    public WebElement productBtn;
    @FindBy(how = How.ID, using = "pl.ceneo:id/bar_title")
    public WebElement barTitle;
    @FindBy(how = How.ID, using = "pl.ceneo:id/fab_icon_wrapper")
    public WebElement moreActionBtn;
    @FindBy(how = How.XPATH, using = "//*[contains(@text,'Wyczyść listę')]")
    public WebElement clearList;
    @FindBy(how = How.ID, using = "android:id/button1")
    public WebElement acceptBtn;


//    @FindBy(how = How.ID, using = "pl.ceneo:id/barcodeMenuItem")
//    public WebElement codeScannerBtn;
//    @FindBy(how = How.ID, using = "pl.ceneo:id/barcodeMenuItem")
//    public WebElement codeScannerBtn;









    WebDriver driver = BaseTest.getDriver();
    Helper helper = PageFactory.initElements(driver, Helper.class);
    public void choosingOffert() {

        findBtn.click();
        helper.pauza(800);
        searchTextField.sendKeys("drapak dla Kota");


        helper.pauza(500);
        helper.tapCoordinates(10, 450);
        helper.pauza(1000);


        productBtn.click();
        helper.pauza(2300);
        helper.comeBack();

        String title = barTitle.getAttribute("text");

    }

    public void followingAdd(){

        helper.tapCoordinates(50,1050);
        helper.tapCoordinates(50, 420);
        helper.tapCoordinates(50,1700);

        helper.pauza(1200);
        helper.tapCoordinates(100,150);
        helper.pauza(1200);

        helper.tapCoordinates(82,1050);

        moreActionBtn.click();
        helper.pauza(800);
        clearList.click();
        helper.pauza(800);
        acceptBtn.click();
        helper.pauza(1200);

        helper.tapCoordinates(100,150);
        helper.pauza(1200);
        helper.tapCoordinates(82,1050);
        helper.pauza(1200);

//        filterButton.click();
//        categoryListBtn.click();



//        catFoodAndAccesory.click();
//        helper.pauza(800);
//        showProductsButton.click();
//        helper.pauza(1200);





    }
}
