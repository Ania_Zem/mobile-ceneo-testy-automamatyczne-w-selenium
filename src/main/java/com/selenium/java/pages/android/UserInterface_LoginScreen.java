package com.selenium.java.pages.android;

import com.selenium.java.base.BaseTest;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
public class UserInterface_LoginScreen extends BaseTest {
    @FindBy(how = How.XPATH, using = "//android.widget.ImageButton[@content-desc=\"menu otwarte\"]\n")
    public WebElement btn_menu;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Zaloguj')]")
    public WebElement btn_log;
    @FindBy(how = How.ID, using = "pl.ceneo:id/ceneo_login_button")
    public WebElement btn_ceneo;
    @FindBy(how = How.ID, using = "pl.ceneo:id/loginTextView")
    public WebElement input_email;
    @FindBy(how = How.ID, using = "pl.ceneo:id/passwordTextView")
    public WebElement input_pass;
    @FindBy(how = How.ID, using = "pl.ceneo:id/loginButton")
    public WebElement btn_login;


    public void loginToApp(String email, String pass) {
        btn_menu.click();
        btn_log.click();
        btn_ceneo.click();
        input_email.sendKeys(email);
        input_pass.sendKeys(pass);
        btn_login.click();
    }
}