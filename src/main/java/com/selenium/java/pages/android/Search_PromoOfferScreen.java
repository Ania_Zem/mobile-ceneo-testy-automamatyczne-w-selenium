package com.selenium.java.pages.android;

import com.selenium.java.base.BaseTest;
import com.selenium.java.base.TestListener;
import com.selenium.java.helper.Helper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class Search_PromoOfferScreen extends BaseTest {

    @FindBy(how = How.ID, using = "pl.ceneo:id/showcaseImageView")
    public WebElement mainAdvertisement;

    //// do tego zrobic tap and swipe ///////
    @FindBy(how = How.ID, using = "pl.ceneo:id/left_showcase")
    public WebElement leftExposedAdvert;
    @FindBy(how = How.ID, using = "pl.ceneo:id/right_showcase")
    public WebElement rightExposedAdvert;
    @FindBy(how = How.ID, using = "pl.ceneo:id/showcaseImageView")
    public WebElement mainExposedAdvert;
    @FindBy(how = How.ID, using = "pl.ceneo:id/filterButtonText")
    public WebElement filterBtn;
    @FindBy(how = How.XPATH, using = "//*[contains(@text,'Cena')]")
    public WebElement priceBtn;
    @FindBy(how = How.ID, using = "pl.ceneo:id/fromFilterEdit")
    public WebElement filterPriceFrom;
    @FindBy(how = How.ID, using = "pl.ceneo:id/toFilterEdit")
    public WebElement filterPriceTo;
    @FindBy(how = How.ID, using = "pl.ceneo:id/clear")
    public WebElement clearBtn;
    @FindBy(how = How.ID, using = "pl.ceneo:id/apply")
    public WebElement applyPriceBtn;
    @FindBy(how = How.XPATH, using = "//*[contains(@text,'Kup Teraz')]")
    public WebElement buyNowBtn;
    @FindBy(how = How.ID, using = "pl.ceneo:id/filtersApplyButton")
    public WebElement acceptFiltersBtn;
    @FindBy(how = How.ID, using = "pl.ceneo:id/sortButtonText")
    public WebElement sortBtn;
    @FindBy(how = How.XPATH, using = "//*[contains(@text,'Cena: od najniższej')]")
    public WebElement fromLowestPrice;
    @FindBy(how = How.XPATH, using = "//*[contains(@text,'Cena: od najwyższej')]")
    public WebElement fromHiestPrice;
    @FindBy(how = How.ID, using = "Przejdź wyżej")
    public WebElement backBtn;



    WebDriver driver = BaseTest.getDriver();
    Helper helper = PageFactory.initElements(driver, Helper.class);
    TestListener testListener = PageFactory.initElements(driver, TestListener.class);

    public void searchingMainAd() {

        WebDriver driver = getDriver();
        Helper helper = PageFactory.initElements(driver, Helper.class);

        helper.pauza(2000);
        System.out.println("Zaczynamy risercz");

        helper.pauza(7000);
        mainExposedAdvert.click();
        System.out.println("klikanko w Główne promowane ogłoszenie");
        helper.testScreenShoot("Główna promocja");
        System.out.println("Zrobiony screen głównej promowanej kategorii");
        helper.pauza(1000);

        filterBtn.click();
        helper.pauza(1200);
        helper.testScreenShoot("Menu filtracji");
        System.out.println("Zrobiony screen menu filtracji");

        priceBtn.click();
        filterPriceFrom.sendKeys("220");
        filterPriceTo.sendKeys("2525");

        helper.testScreenShoot("Wpisanie filtru cenowego");
        System.out.println("Zrobiony screen filtru cenowego");
        helper.pauza(1000);
        clearBtn.click();
        helper.testScreenShoot("Wyczyszczenie filtru cenowego za pomocą buttona");
        System.out.println("Zrobiony screen czyszczenia filtru cenowego za pomocą buttona");
    }

    public void filterPriceCheck() {

        filterPriceFrom.sendKeys("220");
        filterPriceTo.sendKeys("2525");
        helper.pauza(2500);
        filterPriceFrom.clear();
        filterPriceTo.clear();

        helper.testScreenShoot("Wyczyszczenie filtru cenowego za pomocą komendy");
        System.out.println("Zrobiony screen czyszczenia filtru cenowego za pomocą komendy");

        helper.pauza(2000);
    }

    public void filterBuyNowAndSort() {
        filterPriceFrom.sendKeys("220");
        filterPriceTo.sendKeys("2525");
        helper.pauza(2500);
        applyPriceBtn.click();
        buyNowBtn.click();
        helper.pauza(1500);
        acceptFiltersBtn.click();
        helper.pauza(2000);

        sortBtn.click();
        fromLowestPrice.click();
//        moreComment.click();

        helper.pauza(1500);

        helper.testScreenShoot("Od najniższej ceny");
        System.out.println("Zrobiony screen sortowania od najniższej ceny");



        sortBtn.click();
        fromHiestPrice.click();
        helper.pauza(1500);
        helper.testScreenShoot("Od najwyższej ceny");
        System.out.println("Zrobiony screen sortowania od najwyższej ceny");

        helper.comeBack();
    }


    public void searchingLeftPromo() {

        leftExposedAdvert.click();
        System.out.println("klikanko w kewe małe ogłoszenie");
        helper.pauza(1000);
        helper.testScreenShoot("Lewa mała promocja");
        System.out.println("Zrobiony screen lewej promowanej kategorii");
        System.out.println("będzie słajpowane");
        helper.swipeToActionPartners();
        System.out.println("po słajpie");


    }

    public void searchingRightPromo() {
        rightExposedAdvert.click();
        System.out.println("klikanko w prawe małe ogłoszenie");
        helper.pauza(1000);
        helper.testScreenShoot("Prawa mała promocja");
        System.out.println("Zrobiony screen prawej promowanej kategorii");
        System.out.println("będzie słajpowane");
        helper.swipeToActionPartners();
        System.out.println("po słajpie");
        helper.comeBack();
    }

}