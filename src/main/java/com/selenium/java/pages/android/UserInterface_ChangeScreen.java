package com.selenium.java.pages.android;

import com.selenium.java.base.BaseTest;
import com.selenium.java.helper.Helper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class UserInterface_ChangeScreen extends BaseTest {

    @FindBy(how = How.ID, using = "pl.ceneo:id/account_edit_name_icon_image_view")
    public WebElement btn_NameChange;
    @FindBy(how = How.ID, using = "pl.ceneo:id/accountEditNickname")
    public WebElement txt_NameChange;
    @FindBy(how = How.ID, using = "pl.ceneo:id/accountNicknameAcceptBtn")
    public WebElement btn_Done;


    WebDriver driver = BaseTest.getDriver();
    Helper helper = PageFactory.initElements(driver, Helper.class);

    public void myUserChange() {
        btn_NameChange.click();
        txt_NameChange.sendKeys("A");
        btn_Done.click();

        helper.pauza(1500);

        txt_NameChange.click();
        txt_NameChange.sendKeys("Test.Paulina");
        btn_Done.click();

        helper.pauza(2000);

        txt_NameChange.click();
        txt_NameChange.sendKeys("Qwertyuiopasdfghjklzxcvbnm");
        btn_Done.click();

        helper.pauza(2000);

        btn_NameChange.click();
        txt_NameChange.sendKeys("Kurwa");
        btn_Done.click();

        helper.pauza(1500);

        btn_NameChange.click();
        txt_NameChange.sendKeys("TestPaulina");
        btn_Done.click();
    }
}