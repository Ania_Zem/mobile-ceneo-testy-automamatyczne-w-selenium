package com.selenium.java.pages.android;
import com.selenium.java.base.BaseTest;
import com.selenium.java.helper.Helper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;


public class Logging_AddNewOffertScreen extends BaseTest {
    @FindBy(how = How.ID, using = "pl.ceneo:id/dontAskMeAgain")
    public WebElement dontAskMeAgainField;
    @FindBy(how = How.ID, using = "android:id/button1")
    public WebElement acceptBtn;
    @FindBy(how = How.XPATH, using = "//android.widget.ImageButton[@content-desc=\"menu otwarte\"]\n")
    public WebElement btn_menu;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Ogłoszenia')]")
    public WebElement btn_adv;
    @FindBy(how = How.ID, using = "pl.ceneo:id/fab")
    public WebElement btn_add;
    @FindBy(how = How.XPATH, using = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/" +
            "android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/androidx." +
            "drawerlayout.widget.DrawerLayout/android.widget.RelativeLayout/android.widget.FrameLayout[2]/" +
            "android.view.ViewGroup/android.webkit.WebView/android.webkit.WebView/android.widget.TabWidget/android.view." +
            "View[6]/android.view.View/android.view.View[1]/android.widget.EditText[1]\n")
    public WebElement input_title;
    @FindBy(how = How.XPATH, using = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget." +
            "FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/androidx.drawerlayout.widget.DrawerLayout/android." +
            "widget.RelativeLayout/android.widget.FrameLayout[2]/android.view.ViewGroup/android.webkit.WebView/android.webkit.WebView/" +
            "android.widget.TabWidget/android.view.View[6]/android.view.View/android.view.View[1]/android.widget.EditText[2]\n")
    public WebElement input_description;
    @FindBy(how = How.XPATH, using = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget." +
            "FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/androidx.drawerlayout.widget.DrawerLayout/" +
            "android.widget.RelativeLayout/android.widget.FrameLayout[2]/android.view.ViewGroup/android.webkit.WebView/android.webkit.WebView" +
            "/android.widget.TabWidget/android.view.View[6]/android.view.View/android.view.View[1]/android.view.View[5]/android.view.View[2]/android" +
            ".widget.EditText\n")
    public WebElement input_price;
    @FindBy(how = How.XPATH, using = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android." +
            "widget.LinearLayout/android.widget.FrameLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.RelativeLayout/android.widget." +
            "FrameLayout[2]/android.view.ViewGroup/android.webkit.WebView/android.webkit.WebView/android.widget.TabWidget/android.view.View[6]/android." +
            "view.View/android.view.View[1]/android.view.View[6]/android.widget.Button\n")
    public WebElement btn_next;
    @FindBy(how = How.XPATH, using = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/" +
            "android.widget.LinearLayout/android.widget.FrameLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.RelativeLayout/" +
            "android.widget.FrameLayout[2]/android.view.ViewGroup/android.webkit.WebView/android.webkit.WebView/android.widget.TabWidget/android." +
            "view.View[6]/android.view.View/android.view.View/android.view.View/android.view.View\n")
    public WebElement btn_searchphoto;
    @FindBy(how = How.XPATH, using = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android." +
            "widget.FrameLayout/android.widget.FrameLayout/androidx.appcompat.widget.LinearLayoutCompat/android.widget.FrameLayout/android.widget" +
            ".ListView/android.widget.TextView[1]\n")
    public WebElement btn_gallery;
    @FindBy(how = How.XPATH, using = "//*[contains(@text,'Messenger')]")
    public WebElement btn_folder;
    @FindBy(how = How.XPATH, using = "(//android.widget.FrameLayout[@content-desc=\"Button\"])[1]/android.widget.ImageView\n")
    public WebElement btn_photo;
    @FindBy(how = How.XPATH, using = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android" +
            ".widget.RelativeLayout/android.widget.LinearLayout[1]/android.widget.Button[2]\n")
    public WebElement btn_choose;
    @FindBy(how = How.XPATH, using = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget." +
            "FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget." +
            "RelativeLayout/android.widget.FrameLayout[2]/android.view.ViewGroup/android.webkit.WebView/android.webkit.WebView/android.widget." +
            "TabWidget/android.view.View[6]/android.view.View/android.widget.Button[2]\n")
    public WebElement btn_next2;
    @FindBy(how = How.XPATH, using = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/" +
            "android.widget.LinearLayout/android.widget.FrameLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.RelativeLayout/" +
            "android.widget.FrameLayout[2]/android.view.ViewGroup/android.webkit.WebView/android.webkit.WebView/android.widget.TabWidget/android.view." +
            "View[6]/android.view.View/android.widget.ListView/android.view.View[14]\n")
    public WebElement btn_fashion;
    @FindBy(how = How.XPATH, using = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget." +
            "LinearLayout/android.widget.FrameLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.RelativeLayout/android.widget.FrameLayout" +
            "[2]/android.view.ViewGroup/android.webkit.WebView/android.webkit.WebView/android.widget.TabWidget/android.view.View[6]/android.view.View/" +
            "android.widget.ListView/android.view.View[1]\n")
    public WebElement btn_kids;
    @FindBy(how = How.XPATH, using = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget" +
            ".LinearLayout/android.widget.FrameLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.RelativeLayout/android.widget.FrameLayout" +
            "[2]/android.view.ViewGroup/android.webkit.WebView/android.webkit.WebView/android.widget.TabWidget/android.view.View[6]/android.view.View/" +
            "android.widget.ListView/android.view.View[4]\n")
    public WebElement btn_clothes;
    @FindBy(how = How.XPATH, using = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget." +
            "LinearLayout/android.widget.FrameLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.RelativeLayout/android.widget.FrameLayout" +
            "[2]/android.view.ViewGroup/android.webkit.WebView/android.webkit.WebView/android.widget.TabWidget/android.view.View[6]/android.view.View/" +
            "android.widget.Button[2]\n")
    public WebElement btn_next3;
    @FindBy(how = How.XPATH, using = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget." +
            "LinearLayout/android.widget.FrameLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.RelativeLayout/android.widget.FrameLayout" +
            "[2]/android.view.ViewGroup/android.webkit.WebView/android.webkit.WebView/android.widget.TabWidget/android.view.View[6]/android.view.View/" +
            "android.view.View/android.view.View[1]/android.widget.EditText\n")
    public WebElement input_name;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Dalej')]")
    public WebElement btn_done;


    WebDriver driver = BaseTest.getDriver();
    Helper helper = PageFactory.initElements(driver, Helper.class);
    public void addToApp(String title, String description, String price, String name) {

        dontAskMeAgainField.click();
        helper.pauza(400);
        acceptBtn.click();
        helper.pauza(600);

        btn_menu.click();
        System.out.println("1");
        btn_adv.click();
        System.out.println("2");
        btn_add.click();
        System.out.println("3");

        helper.pauza(6000);

        System.out.println("opis");
        input_title.sendKeys(title);
        helper.pauza(6000);

        System.out.println("działa ??");
        input_description.sendKeys(description);
        helper.pauza(6000);

        System.out.println("działa 2 ??");

        input_price.sendKeys(price);
        System.out.println("działa 3 ??");
        btn_next.click();
        System.out.println("działa 4 ??");
        btn_searchphoto.click();

//        btn_gallery.sendKeys("/storage/emulated/0/Pictures/Messenger/received_962751967481289.jpeg");
        btn_gallery.click();
        while (helper.swipeToElementByText("Messenger")) {
            helper.swipeInDirection(Helper.direction.UP, "up", 0.4); //od 0.2 do 0.8
        }
        btn_folder.click();
        System.out.println("działa 5 ??");
        btn_photo.click();
        System.out.println("działa 6 ??");
        btn_choose.click();
        System.out.println("działa 7 ??");
        helper.pauza(6000);

        btn_next2.click();
        System.out.println("działa 8 ??");
        btn_fashion.click();
        System.out.println("działa 9 ??");
        btn_kids.click();
        System.out.println("działa 10 ??");
        btn_clothes.click();
        System.out.println("działa 11 ??");
        btn_next3.click();
        System.out.println("działa 12 ??");
        input_name.sendKeys(name);
        System.out.println("działa 13 ??");

        helper.tapCoordinates(955,1545);
        System.out.println("działa 14 ??");
        helper.pauza(3000);
        btn_done.click();
        System.out.println("działa 15 ??");
    }
}
