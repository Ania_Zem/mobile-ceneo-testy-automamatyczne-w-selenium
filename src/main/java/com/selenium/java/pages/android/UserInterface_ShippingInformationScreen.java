package com.selenium.java.pages.android;

import com.selenium.java.base.BaseTest;
import com.selenium.java.helper.Helper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class UserInterface_ShippingInformationScreen extends BaseTest {

    @FindBy(how = How.XPATH, using = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget." +
            "FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/androidx.drawerlayout.widget." +
            "DrawerLayout/android.widget.RelativeLayout/android.widget.FrameLayout[2]/android.widget." +
            "RelativeLayout/android.widget.ScrollView/android.widget.RelativeLayout/android.widget.LinearLayout[6]/android.widget.RelativeLayout\n")
    public WebElement btn_ShippingInfo;
    @FindBy(how = How.ID, using = "pl.ceneo:id/fab")
    public WebElement btn_AddNew;
    @FindBy(how = How.ID, using = "pl.ceneo:id/arrow")
    public WebElement btn_BackEdit;
    @FindBy(how = How.ID, using = "pl.ceneo:id/firstName")
    public WebElement txt_FirstName;
    @FindBy(how = How.ID, using = "pl.ceneo:id/lastName")
    public WebElement txt_LastName;
    @FindBy(how = How.ID, using = "pl.ceneo:id/streetAndNumber")
    public WebElement txt_StreetAndNumber;
    @FindBy(how = How.ID, using = "pl.ceneo:id/postalCode")
    public WebElement txt_PostalCode;
    @FindBy(how = How.ID, using = "pl.ceneo:id/city")
    public WebElement txt_City;
    @FindBy(how = How.ID, using = "pl.ceneo:id/phoneNumber")
    public WebElement txt_PhoneNumber;
    @FindBy(how = How.ID, using = "pl.ceneo:id/applyBtn")
    public WebElement btn_Apply;


    WebDriver driver = BaseTest.getDriver();
    Helper helper = PageFactory.initElements(driver, Helper.class);
    public void myShippingInformationCorrect() {
//        Informacje do wysyłki
        btn_ShippingInfo.click();
        btn_AddNew.click();
        txt_FirstName.sendKeys("Paulina");
        txt_LastName.sendKeys("Mikler");
        txt_StreetAndNumber.sendKeys("Kazimierza Wielkiego 10/45");
        txt_PostalCode.sendKeys("89-100");
        txt_City.sendKeys("Nakło nad Notecią");
        txt_PhoneNumber.sendKeys("789789789");
        btn_Apply.click();
    }
    public void myShippingInformationIncorrectName() {
//        Edycja informacji do wysyłki z błędami
        btn_BackEdit.click();
        txt_FirstName.sendKeys("Paulina Mikler");
        txt_LastName.sendKeys("Paulina Mikler");
        txt_StreetAndNumber.sendKeys("Kazimierza Wielkiego");

        helper.pauza(2000);

        txt_StreetAndNumber.sendKeys("Kazimierza Wielkiego 12/45");
        txt_PostalCode.sendKeys("8-9100");

        helper.pauza(2000);

        txt_PostalCode.sendKeys("89-100");
        txt_City.sendKeys("Nakło nad Notecią");
        txt_PhoneNumber.sendKeys("78978978978");

        helper.pauza(1500);
        txt_PhoneNumber.sendKeys("789789789");
        btn_Apply.click();
    }
    public void myShippingInformationIncorrectCode() {
//        Błędny kod pocztowy(nie do tego miasta)
        btn_BackEdit.click();
        txt_FirstName.sendKeys("Kurwa");
        txt_LastName.sendKeys("Kurwa");
        txt_StreetAndNumber.sendKeys("Powstańców Wielkopolskich 10/25");
        txt_PostalCode.sendKeys("89-100");
        txt_City.sendKeys("Koronowo");
        txt_PhoneNumber.sendKeys("789789789");
        btn_Apply.click();
    }
    public void myShippingInformationCorrectEdit() {
//        Poprawne informacje
        btn_BackEdit.click();
        txt_FirstName.sendKeys("Paulina");
        txt_LastName.sendKeys("Mikler");
        txt_StreetAndNumber.sendKeys("Powstańców Wielkopolskich 10/25");
        txt_PostalCode.sendKeys("86-010");
        txt_City.sendKeys("Koronowo");
        txt_PhoneNumber.sendKeys("789789789");
        btn_Apply.click();
    }
}