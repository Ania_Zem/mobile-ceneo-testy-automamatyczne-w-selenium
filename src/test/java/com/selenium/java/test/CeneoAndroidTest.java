package com.selenium.java.test;

import com.selenium.java.base.BaseTest;
import com.selenium.java.helper.Helper;
import com.selenium.java.pages.android.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestContext;
import org.testng.annotations.*;

import static org.testng.Assert.assertEquals;

public class CeneoAndroidTest extends BaseTest {

    @BeforeClass
    public void startup() {
        startAppiumServer();
    }

    @BeforeMethod
    @Parameters(value = {"Platform", "DeviceID", "DeviceName"})
    public void setUp(String platform, String deviceId, String deviceName, ITestContext context) {
        instalApp = false;
        launchAndroid(platform, deviceId, deviceName, context);
    }

    Helper helper = new Helper();

    @DataProvider
    public Object[][] getLogin() {
        return new Object[][]{
                {"testerkurs01@gmail.com", "FaceAccount1"},
                {"testerkurs01@gmail.com", "FaceAccount1"},
                {"testerkurs01@gmail.com", "FaceAccount1"},
                {"testerkurs01@gmail.com", "FaceAccount1"},
                {"testerkurs01@gmail.com", "FaceAccount1"},
                {"testerkurs01@gmail.com", "FaceAccount1"},
                {"testerkurs01@gmail.com", "FaceAccount1"},
                {"testerkurs01@gmail.com", "FaceAccount1"},
                {"testerkurs01@gmail.com", "FaceAccount1"},
                {"testerkurs01@gmail.com", "FaceAccount1"}
        };
    }

    @DataProvider
    public Object[][] getFalseLogin() {
        return new Object[][]{
                {"testerkurs01@gmail.com", "FaceAccount1"},
                {"testeprkurs01@gmail.com", "FaceAccount1"},
                {"testerakurs01@gmail.com", "FaceAccount1"},
                {"testerkwurs01@gmail.com", "FaceAccount1"},
                {"testerkulrs01@gmail.com", "FaceAccount1"},
                {"testerkurs01@gmail.com", "FaceAccount1"},
                {"testerkurs01@gmail.com", "FaceAccount1"},
                {"testerkurs01@gmail.com", "FaceAccount1"},
                {"testerkurs01@gmail.com", "FaceAccount1"},
                {"testerkurs01@gmail.com", "FaceAccount1"}
        };
    }

    @DataProvider
    public Object[][] getFalsePass() {
        return new Object[][]{
                {"testerkurs01@gmail.com", "FasceAccount1"},
                {"testerkurs01@gmail.com", "FacaeAccount1"},
                {"testerkurs01@gmail.com", "FaceadAccount1"},
                {"testerkurs01@gmail.com", "FaceAcdswcount1"},
                {"testerkurs01@gmail.com", "FaceAccouwent1"},
                {"testerkurs01@gmail.com", "FaceadAccount1"},
                {"testerkurs01@gmail.com", "FaceadAccount1"},
                {"testerkurs01@gmail.com", "FaceadAccount1"},
                {"testerkurs01@gmail.com", "FaceadAccount1"},
                {"testerkurs01@gmail.com", "FaceadAccount1"}
        };
    }

    @DataProvider
    public Object[][] getAdd() {
        return new Object[][]{
                {"Komrza ministrancka", "Komrza ministrancka dla dziecka 11 lat. 152cm.", "86", "Tester"}
        };
    }

    @Test
    public void test1_Registration() {

        WebDriver driver = getDriver();

        Logging_RegistrationScreen loggingRegistrationScreen = PageFactory.initElements(driver, Logging_RegistrationScreen.class);
        loggingRegistrationScreen.registrationToApp("testerkurs01@gmail.com", "FaceAccount1", "FaceAccount1");

        helper.getScreenShot("Registration Screenshot");
        helper.testScreenShoot("Registration Screenshot 2");
    }

    @Test(dataProvider = "getLogin")
    public void test2_Login(String email, String pass) {

        WebDriver driver = getDriver();

        UserInterface_LoginScreen userInterfaceLoginScreen = PageFactory.initElements(driver, UserInterface_LoginScreen.class);
        userInterfaceLoginScreen.loginToApp(email, pass);

        helper.getScreenShot("poprawne logowanie");
        helper.testScreenShoot("poprawne logowanie");
    }

    @Test(dataProvider = "getFalseLogin")
    public void test3_FalseLogin(String email, String pass) {

        WebDriver driver = getDriver();

        UserInterface_LoginScreen userInterfaceLoginScreen = PageFactory.initElements(driver, UserInterface_LoginScreen.class);
        userInterfaceLoginScreen.loginToApp(email, pass);

        helper.getScreenShot("zły email");
        helper.testScreenShoot("zły email");
    }

    @Test(dataProvider = "getFalsePass")
    public void test4_FalsePass(String email, String pass) {

        WebDriver driver = getDriver();

        UserInterface_LoginScreen userInterfaceLoginScreen = PageFactory.initElements(driver, UserInterface_LoginScreen.class);
        userInterfaceLoginScreen.loginToApp(email, pass);

        helper.getScreenShot("złe hasło");
        helper.testScreenShoot("złe hasło");
    }

    @Test(dataProvider = "getAdd")
    public void test5_AddOfert(String title, String description, String price, String name) {

        WebDriver driver = getDriver();

        UserInterface_LoginScreen userInterfaceLoginScreen = PageFactory.initElements(driver, UserInterface_LoginScreen.class);
        Logging_AddNewOffertScreen logging_addNewOffertScreen = PageFactory.initElements(driver, Logging_AddNewOffertScreen.class);

        userInterfaceLoginScreen.loginToApp("testerkurs01@gmail.com", "FaceAccount1");
        logging_addNewOffertScreen.addToApp(title, description, price, name);
    }

    @Test
    public void test6_NameChangeTest() {
        WebDriver driver = getDriver();
        UserInterface_MainScreen userInterfaceMainScreen = PageFactory.initElements(driver, UserInterface_MainScreen.class);
        UserInterface_ChangeScreen userChange = PageFactory.initElements(driver, UserInterface_ChangeScreen.class);
        userInterfaceMainScreen.myUserInterface();
        userChange.myUserChange();
    }
    @Test
    public void test7_PasswordChangeTest() {
        WebDriver driver = getDriver();
        UserInterface_MainScreen userInterfaceMainScreen = PageFactory.initElements(driver, UserInterface_MainScreen.class);
        Logging_PasswordChangeScreen loggingPasswordChangeScreen = PageFactory.initElements(driver, Logging_PasswordChangeScreen.class);
        userInterfaceMainScreen.myUserInterface();
        loggingPasswordChangeScreen.myPasswordChange();
    }
    @Test
    public void test8_ShippingInformationChangeTest() {
        WebDriver driver = getDriver();
        UserInterface_MainScreen userInterfaceMainScreen = PageFactory.initElements(driver, UserInterface_MainScreen.class);
        UserInterface_ShippingInformationScreen userInterfaceShippingInformationScreen = PageFactory.initElements(driver, UserInterface_ShippingInformationScreen.class);
        userInterfaceMainScreen.myUserInterface();
        userInterfaceShippingInformationScreen.myShippingInformationCorrect();
        userInterfaceShippingInformationScreen.myShippingInformationIncorrectName();
        userInterfaceShippingInformationScreen.myShippingInformationIncorrectCode();
        userInterfaceShippingInformationScreen.myShippingInformationCorrectEdit();
    }

    @Test
    public void test9_PromoAdvertsTest(){

        WebDriver driver = getDriver();
        Search_PromoOfferScreen search_promoOfferScreen = PageFactory.initElements(driver, Search_PromoOfferScreen.class);

        search_promoOfferScreen.searchingMainAd();

        pause(1500);
        String emptyFromPriceField = search_promoOfferScreen.filterPriceFrom.getAttribute("text");
        assertEquals(emptyFromPriceField, "od");
        String emptyToPriceField = search_promoOfferScreen.filterPriceTo.getAttribute("text");
        assertEquals(emptyToPriceField, "do");

        search_promoOfferScreen.filterPriceCheck();

        pause(1000);
        String emptyFromPriceField2 = search_promoOfferScreen.filterPriceFrom.getAttribute("text");
        assertEquals(emptyFromPriceField2, "od");
        String emptyToPriceField2 = search_promoOfferScreen.filterPriceTo.getAttribute("text");
        assertEquals(emptyToPriceField2, "do");

        search_promoOfferScreen.filterBuyNowAndSort();


    }
    @Test
    public void test10_SmallRightPromoAdvert(){
        WebDriver driver = getDriver();
        Search_PromoOfferScreen search_promoOfferScreen = PageFactory.initElements(driver, Search_PromoOfferScreen.class);

        search_promoOfferScreen.searchingRightPromo();
    }
    @Test
    public void test11_SmallLeftPromoAdvert(){
        WebDriver driver = getDriver();
        Search_PromoOfferScreen search_promoOfferScreen = PageFactory.initElements(driver, Search_PromoOfferScreen.class);

        search_promoOfferScreen.searchingLeftPromo();
    }

    @Test
    public void test12_SearchingByFoto() {
        WebDriver driver = getDriver();
        Android.Search_MainCeneoScreen searchMainCeneoScreen = PageFactory.initElements(driver, Android.Search_MainCeneoScreen.class);

        searchMainCeneoScreen.codeScannerByFoto();

    }
    @Test
    public void test13_SearchingByNumbers() {
        WebDriver driver = getDriver();
        Android.Search_MainCeneoScreen searchMainCeneoScreen = PageFactory.initElements(driver, Android.Search_MainCeneoScreen.class);

        searchMainCeneoScreen.codeScannerByNumbers();
        searchMainCeneoScreen.addProductOpinion();

        Boolean Display  = searchMainCeneoScreen.addProductOpinionBtn.isDisplayed();
        assertEquals(Display, "false");
    }

        @Test
    public void test14_CatToys() {
        WebDriver driver = getDriver();
        Search_FindingCatsToys searchFindingCatsToys = PageFactory.initElements(driver, Search_FindingCatsToys.class);

        searchFindingCatsToys.choosingOffert();

        String title = searchFindingCatsToys.barTitle.getAttribute("text");
        assertEquals(title, "\"drapak dla kota\" w \"Drapaki dla kotów\"");

        searchFindingCatsToys.followingAdd();

        String title2 = searchFindingCatsToys.barTitle.getAttribute("text");
        assertEquals(title2, "Ulubione (0)");


        helper.getScreenShot("dodawanie ogłoszenia");
        helper.testScreenShoot("dodawnanie ogłoszenia");

    }
    @Test
    public void test15_CategoryPc() {
        WebDriver driver = getDriver();
        Search_CategoryScreen search_categoryScreen = PageFactory.initElements(driver, Search_CategoryScreen.class);

        search_categoryScreen.computerOffertCategory();
        String title = search_categoryScreen.barTitle.getAttribute("text");
        assertEquals(title, "Edytory grafiki i video");
    }

    @Test
    public void test16_CategoryBook() {
        WebDriver driver = getDriver();
        Search_CategoryScreen search_categoryScreen = PageFactory.initElements(driver, Search_CategoryScreen.class);

        search_categoryScreen.booksOffertCategory();
        String title2 = search_categoryScreen.barTitle.getAttribute("text");
        assertEquals(title2, "Aplikacje biurowe");

    }


    @AfterMethod
    public void afterTest() {
        System.out.println("koniec...");
    }
}